SavedData = {}
SavedData.__index = SavedData

function SavedData:new()

    local FILE_NAME_SETTINGS <const> = "settings"

    -- Make sure these match what you save them as when you call datastore.write
    local KEY_HIGHSCORE <const> = "highscore"
    local KEY_INVERTED <const> = "isInverted"
    local KEY_MUSIC <const> = "musicPlaying"

    local inverted = false
    local music = true
    local highscore = 0

    -- when no filename is passed, default is "data"
    local highScoreData = playdate.datastore.read()
    local settingsData = playdate.datastore.read(FILE_NAME_SETTINGS)

    if highScoreData ~= nil then
        highscore = highScoreData[KEY_HIGHSCORE]
        
        if highscore == nil then 
            highscore = 0 
        end
    end
    
    if settingsData ~= nil then
        inverted = settingsData[KEY_INVERTED]
        music = settingsData[KEY_MUSIC]
        
        if inverted == nil then 
            inverted = false 
        end
        
        if music == nil then 
            music = true 
        end
    end

    -- Call this after any setting changes
    local function updateSettingsWithLocal()
        
    end

    function self:getHighscore()
        return highscore
    end

    function self:isInverted()
        return inverted
    end
    
    function self:musicPlaying()
        return music
    end

    function self:updateHighscoreIfHigher(score)
        if score > highscore then
            highscore = score
            playdate.datastore.write({ highscore = score })
        end
    end

    function self:updateInverted(isInverted)
        inverted = isInverted
        
        playdate.datastore.write({isInverted = inverted}, FILE_NAME_SETTINGS)
    end
    
    function self:updateMusic(isPlaying)
        music = isPlaying
        
        playdate.datastore.write({musicPlaying = music}, FILE_NAME_SETTINGS)
    end

    return self
end
