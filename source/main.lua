-- SNAKE GAME

import 'CoreLibs/graphics'
import 'saveddata'
import 'music'
import 'snake'

pdmenu = playdate.getSystemMenu()

local gfx <const> = playdate.graphics
local savedData <const> = SavedData:new()
local music <const> = Music:new()
local snake <const> = Snake:new()
local eatSound <const> = playdate.sound.sampleplayer.new('assets/sounds/eat')

gfx.setImageDrawMode(gfx.kDrawModeFillWhite)

local lastDirection = 'right'
local point = {}
local score = 0
local gameRunning = true
local initState = true
local frameCountVelocity = 0
local movementBlocked = false -- This prevents snake eating itself when fast turning
local difficulty = 4 -- from 4(low) to 1(high)

playdate.display.setInverted(savedData:isInverted())

if savedData:musicPlaying() then
	music:play()
end

function buttonPressed()
	if (playdate.buttonIsPressed(playdate.kButtonRight) and lastDirection ~= 'left' and movementBlocked == false) then
		lastDirection = 'right'
		movementBlocked = true
	end
	
	if (playdate.buttonIsPressed(playdate.kButtonLeft) and lastDirection ~= 'right' and movementBlocked == false) then
		lastDirection = 'left'
		movementBlocked = true
	end
	
	if (playdate.buttonIsPressed(playdate.kButtonUp) and lastDirection ~= 'down' and movementBlocked == false) then
		lastDirection = 'up'
		movementBlocked = true
	end
	
	if (playdate.buttonIsPressed(playdate.kButtonDown) and lastDirection ~= 'up' and movementBlocked == false) then
		lastDirection = 'down'
		movementBlocked = true
	end
end

function restart()
	snake:reset()
	lastDirection = 'right'
	point = {}
	score = 0
	difficulty = 4
	initState = true
	gameRunning = true
	
	if pdmenu:getMenuItems()[2]:getValue() then
		music:replay()
	end
end

function gameOver()
	gameRunning = false
	if pdmenu:getMenuItems()[2]:getValue() then
		music:gracefullyStop()
	end

	savedData:updateHighscoreIfHigher(score)

	local highscore = savedData:getHighscore()

	gfx.setColor(gfx.kColorWhite)
	gfx.fillRoundRect(75, 50, 250, 140, 10)

	gfx.setColor(gfx.kColorBlack)
	gfx.drawRoundRect(75, 50, 250, 140, 10)
	gfx.fillRoundRect(80, 55, 240, 130, 10)
	gfx.drawText('*GAME OVER*', 400 / 2 - gfx.getTextSize('*GAME OVER*') / 2, 70)

	gfx.drawText('Score:', 140, 100)
	gfx.drawText(tostring(score), 260 - gfx.getTextSize(tostring(score)), 100)

	gfx.drawText('Highscore:', 140, 120)
	gfx.drawText(tostring(highscore), 260 - gfx.getTextSize(tostring(highscore)), 120)

	gfx.drawText('Press A to restart', 400 / 2 - gfx.getTextSize('Press A to restart') / 2, 155)

	if playdate.buttonIsPressed(playdate.kButtonA) then restart() end

end

function moveSnake()
	snake:move(lastDirection)
end

function printOnScreen()
	gfx.clear()
	gfx.fillRect(snake:getX(), snake:getY(), 10, 10) -- This is the snake's head
	gfx.fillRect(point[1], point[2], 10, 10) -- This is the point the snake has to eat

	snake:displayTail(gfx) -- This is the snake's body
end

function playdate.update()
	buttonPressed()

	if (gameRunning == true) then
		if (frameCountVelocity == difficulty) then
			moveSnake()
			
			if (snake:OutOfScreen() == true or snake:eatenItself() == true) then
				gameOver()
			end
			
			if (snake:pointEaten(point, initState) == true) then
				eatSound:play()
				score += 1
				if (difficulty > 1 and snake:getTailLength() % 5 == 0) then
					difficulty -= 1
				end
			else
				initState = false
			end
			
			printOnScreen()
			movementBlocked = false
			frameCountVelocity = 0
		else
			frameCountVelocity += 1
		end
	else
		gameOver()
	end
end

pdmenu:addCheckmarkMenuItem('Inverted', savedData:isInverted(), function(value)
	savedData:updateInverted(value)
  playdate.display.setInverted(value)
end)

pdmenu:addCheckmarkMenuItem('Music', savedData:musicPlaying(), function(value)
	savedData:updateMusic(value)
	if value == true then
		music:setRate(1)
		music:play()
	else
		music:stop()
	end
end)
