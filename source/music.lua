Music = {}

function Music:new()
	
	local backgroundSound = playdate.sound.fileplayer.new("assets/music/background")
	
	function self:play()
		backgroundSound:play(0)
	end
	
	function self:setRate(rate)
		backgroundSound:setRate(rate)
	end
	
	function self:replay()
		backgroundSound:setRate(1)
		backgroundSound:setOffset(0)
		backgroundSound:stop()
		backgroundSound:play(0)
	end
	
	function self:gracefullyStop()
		backgroundSound:setRate(backgroundSound:getRate() - 0.1)
	end
	
	function self:stop()
		backgroundSound:stop()
	end
	
	function self:isPlaying()
		return backgroundSound:isPlaying()
	end
	
	return self
end
