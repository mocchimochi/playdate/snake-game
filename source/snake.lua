Snake = {}

function Snake:new()
	
	local pos = {
		x = 0,
		y = 0
	}
	local tail = {} -- The snake body coords
	
	function self:reset()
		pos['x'] = 0
		pos['y'] = 0
		tail = {}
	end
	
	function self:getX()
		return pos['x']
	end
	
	function self:getY()
		return pos['y']
	end
	
	function self:getTailLength()
		return #tail
	end
	
	function self:move(lastDirection)
		if (lastDirection == 'right') then
			pos['x'] += 10
		elseif (lastDirection == 'left') then
			pos['x'] -= 10
		elseif (lastDirection == 'up') then
			pos['y'] -= 10
		elseif (lastDirection == 'down') then
			pos['y'] += 10
		end
	end
	
	function self:OutOfScreen()
		if pos['x'] < 0 or pos['x'] >= 400 or pos['y'] < 0 or pos['y'] >= 240 then
			return true
		end
		
		return false
	end
	
	function self:eatenItself()
		for i = 1, #tail do
			if pos['x'] == tail[i][1] and pos['y'] == tail[i][2] then
				return true
			end
		end
		
		return false
	end
	
	function self:displayTail(gfx)
		for i = 1, #tail do
			if i == #tail then
				tail[#tail + 1 - i][1] = pos['x']
				tail[#tail + 1 - i][2] = pos['y']
			else
				tail[#tail + 1 - i][1] = tail[#tail - i][1]
				tail[#tail + 1 - i][2] = tail[#tail - i][2]
			end
		end
		for i = 1, #tail do
			gfx.fillRect(tail[i][1], tail[i][2], 10, 10)
		end
	end
	
	function self:pointEaten(point, initState)
		if pos['x'] == point[1] and pos['y'] == point[2] or point[1] == nil then
			point[1] = math.random(39) * 10
			point[2] = math.random(23) * 10
			
			table.insert(tail, { pos['x'], pos['y'] })
			
			if initState == false then
				return true
			else
				return false
			end
		end
	end
	
	return self
end
