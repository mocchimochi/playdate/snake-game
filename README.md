# playdate-snake-game

<p align="center">
  <img src="https://i.imgur.com/BntL9Vg.png">
</p>

##### Background music by: [8 Bit And Chill](https://www.patreon.com/8bitandchill)
---

### Download the game [here](https://tauchme.itch.io/simple-snake-playdate)

<p align="right"><a href="https://ko-fi.com/K3K281ONV" target="_blank"><img src="https://ko-fi.com/img/githubbutton_sm.svg" alt="ko-fi"></a></p>
